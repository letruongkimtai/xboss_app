import { StyleSheet } from 'react-native';

export const login = StyleSheet.create({
    form: {
        flex: 5,
        marginTop: 20,
    },
    formContainer: {
        flex: 1,
        alignItems: 'center',
    },
    infoInput: {
        height: "12%",
        width: "78%",
        marginTop: 20,
        fontSize: 17,
        color: "white"
    },
    bottomBorder: {
        borderBottomColor: "#e5e5e5",
        borderBottomWidth: 1,
    },
    sectionMargin: {
        marginTop: 40,
    },
    button: {
        marginLeft: 40,
        marginRight: 38,
        backgroundColor: "#65C8CD",
    },
    googleButton: {
        marginTop: 15,
        marginLeft: 39,
        marginRight: 38,
        width: "81%",
        height: 53,
    },
    buttonTitle: {
        fontWeight: '600',
        color: "white",
        fontFamily: 'OpenSans',
        fontSize: 17,
    },
    googleLoginButton: {
        marginTop: 15,
        marginLeft: 42,
        marginRight: 39,
        width: "79%",
        height: 45,
        backgroundColor: "white",
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        elevation: 3,
        borderRadius: 1,
        flexDirection: 'row',
    },
    googleLogo: {
        flex: 1,
        alignItems: 'flex-end',
        alignContent: 'center',
    },
    googleButtonTitle: {
        flex: 3,
        alignItems: 'flex-start',
        alignContent: 'center',
    }
})