import {
    Dimensions
} from 'react-native'

export const phoneSize = () =>{
    const windowWidth = Dimensions.get('window').width
    const windowHeight = Dimensions.get('window').height
    if(windowWidth>windowHeight){
        return 'Landscape'
    }else if(windowWidth<windowHeight){
        return 'Portrait'
    }
}