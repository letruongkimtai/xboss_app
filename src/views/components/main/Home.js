import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Button,
    Image,
    ToastAndroid,
} from 'react-native';
import { GoogleSignin } from 'react-native-google-signin';


export default class Home extends Component {
    static navigationOptions = {
        header: null,
    }
    constructor(props){
        super(props);
        this.state={
            userInfo:'',
            info:'',
            profileIMG:'',
            loading:false,
        }
    }
    async _getCurrentUser() {
        this.setState({loading:true})
        try {
          const userInfo = await GoogleSignin.signInSilently();
          this.setState({ 
                userInfo, 
                error: null,
                info:userInfo.user.name,
                loading:false ,
                profileIMG:userInfo.user.photo
            });
        } catch (error) {
          const errorMessage =
            error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
          this.setState({
            error: new Error(errorMessage),
          });
        }
      }
    _signOut = async () => {
        const {goBack} = this.props.navigation;
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
    
          this.setState({ userInfo: null, error: null });
          ToastAndroid.show('Goodbye..!',ToastAndroid.SHORT)
          goBack();
        } catch (error) {
          this.setState({
            error,
          });
        }
      };
    _exit = async ()=>{
      const {goBack} = this.props.navigation;
      ToastAndroid.show('Goodbye..!',ToastAndroid.SHORT)
      goBack();
    }
      renderError() {
        const { error } = this.state;
        if (!error) {
          return null;
        }
        const text = `${error.toString()} ${error.code ? error.code : ''}`;
        return <Text>{text}</Text>;
      }
    async componentDidMount() {
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Repleace with your webClientId generated from Firebase console
            webClientId:
                '1067501549968-b4okccqiai8v7bi99kfgm8kq9toac94c.apps.googleusercontent.com',
            offlineAccess: true,
        });
        await this._getCurrentUser();
    }
    render() {
        const {info} = this.state
        const {loading} = this.state
        const {profileIMG} = this.state
        
        if(loading){
            return (
                <View style={home.container}>
                    <Image source={require('../../../assets/images/loading.gif')}/>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 20 }}>
                        Loading your information....
                    </Text>
                </View>
            );
        }
        else{
            return (
                <View style={home.container}>
                    <Text>{profileIMG}</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 20 }}>
                        Welcome {info}
                    </Text>
                    <Button onPress={this._signOut} title="Log out" />
                    <Button 
                      onPress={this._exit} 
                      title="Choose another login method" 
                      style={{backgroundColor:"red",marginTop: 20,}}/>
                    {this.renderError()}
                </View>
            );
        }
    }
}

const home = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
      },
})


