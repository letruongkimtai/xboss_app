import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    Image,
} from 'react-native';
import Form from './Form'

export default class Login extends Component {
    static navigationOptions = {
        header: null,
    }

    

    render() {
        return (
            <ImageBackground
                style={login.Background}
                source={require('../../../assets/images/App-background.png')}
                blurRadius={2}>
                    <View style={login.blurContainer}>
                        <View style={login.logo}>
                            <Image source={require('../../../assets/images/xboss-logo.png')} />
                        </View>
                        <Form homeNav={this.props.navigation}/>
                    </View>
            </ImageBackground>
        );
    }
}

const login = StyleSheet.create({
    Background: {
        width: "100%",
        height: "100%",
    },
    blurContainer: {
        backgroundColor: "rgba(28,117,188,0.8)",
        flex: 1
    },
    logo: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 40,
    },
})
