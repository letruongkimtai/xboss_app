import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Alert,
    ToastAndroid,
} from 'react-native';
import {
    Button,
} from 'native-base'
import{GoogleSignin} from 'react-native-google-signin'
import * as Action from '../../../models/api'
import GoogleLoginButton from './GoogleLoginButton'
import AsyncStorage from '@react-native-community/async-storage'



export default class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            userInfo: null,
            error: null,
            userName: '',
            pass: '',
        };
    }
    async componentDidMount(){
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Repleace with your webClientId generated from Firebase console
            webClientId:
                '1067501549968-b4okccqiai8v7bi99kfgm8kq9toac94c.apps.googleusercontent.com',
            offlineAccess: true,
        });
        const isGoogleSignIn = await GoogleSignin.isSignedIn();
        var userSession = await AsyncStorage.getItem('UserSession')
        if(isGoogleSignIn){
            ToastAndroid.show('Welcome back...!!!',2)
            this.props.homeNav.navigate('HomeScreen');
        }
        // else if(userSession==true){
        //     ToastAndroid.show('Welcome back...!!!',2)
        //     this.props.homeNav.navigate('HomeScreen');      //Cancelled :(
        // }
    }
   
    storeUserData = async (sessionId)=>{
        try {
            await AsyncStorage.setItem('UserSession', sessionId)
            console.log('your data is stored')
          } catch (e) {
            console.log('Something went wrong while store data...')
          }
    }


    handleLoginPress() {
       Action.getUserData(this.state.userName,this.state.pass)
       .then(res=>{
                console.log('=============result=======================');
                console.log(res.result);
                console.log('====================================');
                this.setState({
                    data:res.result,
                })
                // alert('Welcome '+JSON.stringify(this.state.data.name))
                Alert.alert('Welcome', JSON.stringify(this.state.data.name))


                //====================================Testing 1=============================================
                // AsyncStorage.setItem('UserSession',JSON.stringify(this.state.data.session_id))
                // Session =  AsyncStorage.getItem('UserSession')
                // console.log('user session is: ' + Session.toString()) //show object
                //====================================Testing 2=============================================
                // var session = JSON.stringify(this.state.data.session_id)
                // var Session = AsyncStorage.setItem('UserSession',session)
                // console.log('user session is: ' + Session) // still show only object :(
                //====================================Testing 3=============================================
                //storeUserData(JSON.stringify(this.state.data.session_id))
                //====================================Testing 4 - ăn gian test==============================
                // if(this.state.data.session_id!=""){
                //     AsyncStorage.setItem('UserSession',true)   //always true 
                // }
       }).catch(error=>{
           return(
               Alert.alert('What is going on here?????','- Wrong password or username           - Or just simply not entered yet!!!!')
           )
       })
    }
    render() {
        return (
            <View style={login.form}>
                <View style={login.formContainer}>
                    <TextInput style={[login.infoInput, login.bottomBorder]}
                        onChangeText={(text) =>this.setState({userName:text})}
                        placeholder="Username"
                        placeholderTextColor="#e5e5e5" />

                    <TextInput style={[login.infoInput, login.bottomBorder]}
                        onChangeText={(text) => this.setState({pass:text})}
                        placeholder="Passwords"
                        secureTextEntry={true}
                        placeholderTextColor="#e5e5e5" />
                    
                    <Button
                        block style={[login.button, login.sectionMargin]}
                        onPress={() => this.handleLoginPress()}>
                        <Text style={login.buttonTitle}>LOGIN</Text>
                    </Button>
                    <GoogleLoginButton homeNav={this.props.homeNav}/>
                </View>
            </View>
        );
    }
}

const login = StyleSheet.create({
    form: {
        flex: 5,
        marginTop: 20,
    },
    formContainer: {
        flex: 1,
        alignItems: 'center',
    },
    infoInput: {
        height: "12%",
        width: "78%",
        marginTop: 20,
        fontSize: 17,
        color: "white"
    },
    bottomBorder: {
        borderBottomColor: "#e5e5e5",
        borderBottomWidth: 1,
    },
    sectionMargin: {
        marginTop: 40,
    },
    button: {
        marginLeft: 40,
        marginRight: 38,
        backgroundColor: "#65C8CD",
    },
    buttonTitle: {
        fontWeight: '600',
        color: "white",
        fontFamily: 'OpenSans',
        fontSize: 17,
    },
})