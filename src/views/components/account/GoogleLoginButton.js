import React,{Component} from 'react';
import { 
    View,
    Text,
    StyleSheet,
    ToastAndroid,
    Image,
    TouchableOpacity
 } from 'react-native';
 import {
    GoogleSignin,
    statusCodes,
} from 'react-native-google-signin'



export default class GoogleLoginButton extends Component {
    async componentDidMount() {
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Repleace with your webClientId generated from Firebase console
            webClientId:
                '1067501549968-b4okccqiai8v7bi99kfgm8kq9toac94c.apps.googleusercontent.com',
            offlineAccess: true,
        });


    }

    _signIn = async () => {
        //Prompts a modal to let the user sign in into your application.
        try {
            await GoogleSignin.hasPlayServices({
                //Check if device has Google Play Services installed.
                //Always resolves to true on iOS.
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            console.log('User Info --> ', userInfo)
            this.setState({ UserInfo: userInfo })
            
        } catch (error) {
            console.log('Message', error.message);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('Signing In');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log('Play Services Not Available or Outdated');
            } else {
                console.log('Some Other Error Happened');
            }
        }
        const userInfo = await GoogleSignin.getCurrentUser();
        // Alert.alert('current user', userInfo ? JSON.stringify(userInfo.user.email) : 'null');
        if(userInfo){
            this.props.homeNav.navigate('HomeScreen');
            ToastAndroid.show('Wellcome...!!',ToastAndroid.SHORT)
        }
        
    };

    async _getCurrentUser() {
        try {
          const userInfo = await GoogleSignin.signInSilently();
          this.setState({ userInfo, error: null });
        } catch (error) {
          const errorMessage =
            error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
          this.setState({
            error: new Error(errorMessage),
          });
        }
    }
    render() {
        return (
            <TouchableOpacity
                        style={login.googleLoginButton}
                        onPress={this._signIn}>
                        <View style={login.googleLogo}>
                            <Image
                                style={{ marginTop: 5, }}
                                source={require('../../../assets/images/icons8-google-35.png')}
                            />
                        </View>
                        <View style={login.googleButtonTitle}>
                            <Text
                                style={login.buttonTitleContent}>
                                    LOGIN WITH GOOGLE
                            </Text>
                        </View>
            </TouchableOpacity>
        );
    }
}

const login = StyleSheet.create({
    googleLoginButton: {
        marginTop: 15,
        marginLeft: 42,
        marginRight: 39,
        width: "79%",
        height: 45,
        backgroundColor: "white",
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        elevation: 3,
        borderRadius: 1,
        flexDirection: 'row',
    },
    googleLogo: {
        flex: 1,
        alignItems: 'flex-end',
        alignContent: 'center',
    },
    googleButtonTitle: {
        flex: 3,
        alignItems: 'flex-start',
        alignContent: 'center',
    },
    buttonTitleContent:{
        fontSize: 17, 
        fontWeight: '500', 
        color: "#107add", 
        marginTop: 10, 
        marginLeft: 5,
    }
})