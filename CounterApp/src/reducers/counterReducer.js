import { INCREASE, DECREASE } from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';





async function initState() {
  if (data == null || data == 'undefined') {
    const initialState = 0;
    const data = AsyncStorage.getItem('Stored')
    console.log(data);
    return initialState;
  }
  else {
    return initialState = parseInt(data);
  }
}

const initialState = 0;

export default function (state = initialState, action) {
  switch (action.type) {
    case INCREASE:
      return state + 1;

    case DECREASE:
      return state - 1;

    default:
      return state;
  }
}