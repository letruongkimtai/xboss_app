import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Text,
} from 'react-native';
import * as actions from './actions'
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { IncreaseButton, DecreaseButton } from './components/Buttons';

class Main extends Component {
    constructor(props) {
        super(props)
    }

    //storage function.
    saveData = async (data) => {
        await AsyncStorage.setItem(
            'testing', 
            data,
            console.log('Save OK')
        ).catch(e => { console.log(e) })
    }

    getData = async () => {
        try{
            var value = await AsyncStorage.getItem('testing');
            console.log('stored: '+ value.toString());
            return value;
        }catch(e){
            console.log(e);
        }
    }

    handleIncreasePress = () => {
        this.props.increase(); //calling action from ./actions/index.js
        
        //store the value
        // var value = this.props.counter + 1 ;
        // console.log(JSON.stringify(value));
        
        // this.saveData(JSON.stringify(value));
    }
    handleDecreasePress = () => {
        this.props.decrease();

        //store the value
        // var value = this.props.counter - 1 ;
        // console.log(JSON.stringify(value));
        
        // this.saveData(JSON.stringify(value));
        // this.getData();
    }

    render() {
        console.log('number ' + this.props.counter)
        // var number = this.getData();
        // console.log('stored in render: '+ number)
        return (
            <ImageBackground style={styles.backGround} source={require('./assets/images/background.png')}>
                <View style={styles.counterHolder}>
                    <Text style={styles.text}>{this.props.counter}</Text>
                    <IncreaseButton handle={this.handleIncreasePress} />
                    <DecreaseButton handle={this.handleDecreasePress} />
                </View>
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => ({
    counter: state.counter
});

export default connect(mapStateToProps, actions)(Main);//connect action to main component


const styles = StyleSheet.create({
    backGround: {
        height: "100%",
        width: "100%",
        flex: 1,
    },
    counterHolder: {
        flex: 1,
        height: "100%",
        width: "100%",
        alignItems: 'center',
        marginTop: 70,
    },
    text: {
        fontSize: 100,
        color: 'white',
        marginBottom: 90,
    },
})