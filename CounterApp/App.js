import React, { Component } from 'react';
import Main from './src/Main'
import { Provider } from 'react-redux'
import { store, persistor } from './src/store/index'
import { PersistGate } from 'redux-persist/integration/react';

export default class App extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Main/>
        </PersistGate>
      </Provider>
    );
  }
}
